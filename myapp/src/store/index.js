import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name:"你好"
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
